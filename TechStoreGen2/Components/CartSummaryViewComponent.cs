﻿using Microsoft.AspNetCore.Mvc;
using TechStoreGen2.Models;

namespace TechStoreGen2.Components
{
    public class CartSummaryViewComponent : ViewComponent
    {
        private ICart cart;

        public CartSummaryViewComponent(ICart cartService)
        {
            cart = cartService;
        }

        public IViewComponentResult Invoke()
        {
            return View(cart);
        }
    }
}
