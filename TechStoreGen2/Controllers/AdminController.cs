﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using TechStoreGen2.Models;

namespace TechStoreGen2.Controllers
{
    [Authorize(Roles="Admin")]
    public class AdminController : Controller
    {
        private IProductRepository repository;

        public AdminController(IProductRepository repo, UserManager<User> manager)
        {
            repository = repo;
        }
        public IActionResult Index() => View(repository.Products);
        public IActionResult Orders() => RedirectToAction("List", "Order");
        public IActionResult Roles() => RedirectToAction("UserList", "Roles");


        public ViewResult Edit(int productId) =>
            View(repository.Products.FirstOrDefault(p => p.ProductID == productId));

        [HttpPost]
        public IActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                repository.SaveProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                return View(product);
            }
        }

        public ViewResult Create() => View("Edit", new Product());

        [HttpPost]
        public IActionResult Delete(int productID)
        {
            Product deletedProduct = repository.DeleteProduct(productID);
            if (deletedProduct != null)
            {
                TempData["message"] = $"{deletedProduct.Name} was deleted";
            }
            return RedirectToAction("Index");
        }
    }
}
