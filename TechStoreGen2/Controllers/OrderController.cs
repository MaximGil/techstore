﻿using AutoMapper;
using Login.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using TechStoreGen2.Models;

namespace TechStoreGen2.Controllers
{
    public class OrderController : Controller
    {
        private IOrderRepository repository;
        private ICart cart;
        private IMapper mapper;
        public OrderController(IOrderRepository repoService, ICart cartService, IMapper mapp)
        {
            repository = repoService;
            cart = cartService;
            mapper = mapp;
        }

        [Authorize]
        public ViewResult List() =>
            View(repository.Orders.Where(x => !x.Shipped));

        [HttpPost]
        [Authorize]
        public IActionResult MarkShipped(int orderID)
        {
            Order order = repository.Orders.FirstOrDefault(x => x.OrderID == orderID);
            if(order != null)
            {
                order.Shipped = true;
                repository.SaveOrder(order);
            }
            return RedirectToAction(nameof(List));
        }

        public ViewResult Checkout() => View(new Order());

        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }
            if (ModelState.IsValid)
            {
                order.Lines = cart.Lines.ToArray();
                repository.SaveOrder(order);
                return RedirectToAction(nameof(Completed));
            }
            else
            {
                return View(order);
            }
        }

        public ViewResult Completed()
        {
            cart.Clear();
            return View();
        }

        public IActionResult Create(OrderViewModel order)
        {
            if (ModelState.IsValid)
            {
                repository.SaveOrder(mapper.Map<OrderViewModel, Order>(order));
                TempData["message"] = $"{order.Name} has been created";
                return RedirectToAction("Index", "Admin");
            }
            return RedirectToAction("Index", "Admin");
        }

        public IActionResult Edit(OrderViewModel order)
        {
            if (ModelState.IsValid)
            {
                var editedOrder = repository.Orders.FirstOrDefault(i => i.OrderID == order.OrderID);
                editedOrder.Name = order.Name;
                editedOrder.City = order.City;
                editedOrder.Country = order.Country;
                editedOrder.GiftWrap = order.GiftWrap;
                editedOrder.Line1 = order.Line1;
                editedOrder.Line2 = order.Line2;
                editedOrder.Line3 = order.Line3;
                editedOrder.State = order.State;
                editedOrder.Zip = order.Zip;
                repository.SaveOrder(editedOrder);
                TempData["message"] = $"{editedOrder.Name} has been saved";
                return RedirectToAction("Index", "Admin");
            }
            else
            {
                return View(order);
            }
        }

        public IActionResult Delete(int orderID)
        {
            Order deletedOrder = repository.DeleteOrder(orderID);
            if (deletedOrder != null)
            {
                TempData["message"] = $"{deletedOrder.Name} was deleted";
            }
            return RedirectToAction("Index");
        }
    }
}
