﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using TechStoreGen2.Infrastucture;
using TechStoreGen2.Models;
using TechStoreGen2.ViewModels;

namespace TechStoreGen2.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        private IMapper mapper;
        public int PageSize = 8;

        public ProductController(IProductRepository repo, IMapper map)
        {
            repository = repo;
            mapper = map;
        }

        public ViewResult List(string category, int productPage = 1)
             => View(new ProductListViewModel
             {
                 Products = repository.Products
                 .Where(p => category == null || p.Category == category)
                     .OrderBy(p => p.ProductID)
                     .Skip((productPage - 1) * PageSize)
                     .Take(PageSize),
                 PagingInfo = new PagingInfo
                 {
                     CurrentPage = productPage,
                     ItemsPerPage = PageSize,
                     TotalItems = category == null ?
                     repository.Products.Count() :
                     repository.Products.Where(t => t.Category == category).Count()
                 }
             });

        //[Authorize(Roles = "admin, user")]
        //public IActionResult Index()
        //{
        //    string role = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value;
        //    return Content($"your role is: {role}");
        //}

        //[Authorize(Roles = "admin")]
        //public IActionResult About()
        //{
        //    return Content("Sign in for only admin");
        //}

        public IActionResult Create(ProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                repository.SaveProduct(mapper.Map<ProductViewModel, Product>(product));
                TempData["message"] = $"{product.Name} has been created";
                return RedirectToAction("Index", "Admin");
            }
            return RedirectToAction("Index", "Admin");
        }

        public IActionResult Edit(ProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                var prod = repository.Products.FirstOrDefault(i => i.ProductID == product.ProductID);
                prod.Name = product.Name;
                prod.Price = product.Price;
                prod.Category = product.Category;
                prod.Description = product.Description;
                repository.SaveProduct(prod);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index", "Admin");
            }
            else
            {
                return View(product);
            }
        }

        public IActionResult Delete(int productID)
        {
            Product deletedProduct = repository.DeleteProduct(productID);
            if (deletedProduct != null)
            {
                TempData["message"] = $"{deletedProduct.Name} was deleted";
            }
            return RedirectToAction("Index");
        }
    }
}
