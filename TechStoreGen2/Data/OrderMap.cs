﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TechStoreGen2.Models;

namespace TechStoreGen2.Data
{
    public class OrderMap : IEntityTypeConfiguration<OrderDetails>
    {
        public void Configure(EntityTypeBuilder<OrderDetails> builder)
        {
            builder.HasOne(x => x.Product).WithMany(x => x.Orders).HasForeignKey(x => new { x.ProductID });
            builder.HasOne(x => x.Order).WithMany(x => x.OrderDetails).HasForeignKey(x => new { x.OrderID });
        }
    }
}
