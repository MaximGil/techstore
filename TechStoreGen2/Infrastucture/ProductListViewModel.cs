﻿using System.Collections.Generic;
using TechStoreGen2.Models;
using TechStoreGen2.ViewModels;

namespace TechStoreGen2.Infrastucture
{
    public class ProductListViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
    }
}
