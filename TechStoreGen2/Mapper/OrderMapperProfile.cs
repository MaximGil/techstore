﻿using AutoMapper;
using Login.Models.ViewModels;
using TechStoreGen2.Models;

namespace TechStoreGen2.Mapper
{
    public class OrderMapperProfile : Profile
    {
        public OrderMapperProfile()
        {
            CreateMap<OrderViewModel, Order>()
                .ForMember(x => x.Name, y => y.MapFrom(z => z.Name))
                .ForMember(x => x.Line1, y => y.MapFrom(z => z.Line1))
                .ForMember(x => x.Line2, y => y.MapFrom(z => z.Line2))
                .ForMember(x => x.Line3, y => y.MapFrom(z => z.Line3))
                .ForMember(x => x.State, y => y.MapFrom(z => z.State))
                .ForMember(x => x.Zip, y => y.MapFrom(z => z.Zip))
                .ForMember(x => x.City, y => y.MapFrom(z => z.City))
                .ForMember(x => x.Country, y => y.MapFrom(z => z.Country))
                .ForMember(x => x.GiftWrap, y => y.MapFrom(z => z.GiftWrap));
        }
    }
}
