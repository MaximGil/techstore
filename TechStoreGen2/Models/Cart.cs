﻿using Login.Models;
using System.Collections.Generic;
using System.Linq;

namespace TechStoreGen2.Models
{
    public class Cart : ICart
    {
        public virtual void AddItem(Product product, int quantity)
        {
            CartLine line = Lines.Where(p => p.Product.ProductID == product.ProductID).FirstOrDefault();

            if (line == null)
            {
                Lines.Add(new CartLine
                {
                    Product = product,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }
        public virtual void RemoveLine(Product product) =>
        Lines.Remove(GetCartLine(product.ProductID));

        public virtual double ComputeTotalValue() =>
            Lines.Sum(e => e.Product.Price * e.Quantity);

        public virtual void Clear() => Lines.Clear();

        public virtual IList<CartLine> Lines { get; set; } = new List<CartLine>();

        public CartLine GetCartLine(int productID)
        {
            return Lines.FirstOrDefault(el => el.Product.ProductID == productID);
        }
    }
}

