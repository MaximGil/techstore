﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using TechStoreGen2.Data;

namespace TechStoreGen2.Models
{
    public class EFOrderRepository : IOrderRepository
    {
        private ApplicationDbContext context;
        
        public EFOrderRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }
        public IQueryable<Order> Orders => context.Orders.Include(o => o.Lines).ThenInclude(l => l.Product);

        public void SaveOrder(Order order)
        {
            context.AttachRange(order.Lines.Select(l => l.Product));
            if(order.OrderID == 0)
            {
                context.Orders.Add(order);
            }
            context.SaveChanges();
        }

        public Order DeleteOrder(int orderID)
        {
            Order dbEntry = context.Orders.FirstOrDefault(p => p.OrderID == orderID);
            if (dbEntry != null)
            {
                context.Orders.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
