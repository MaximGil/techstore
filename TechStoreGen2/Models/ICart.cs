﻿using System.Collections.Generic;


namespace TechStoreGen2.Models
{
    public interface ICart
    {
         void AddItem(Product product, int quantity);

         void RemoveLine(Product product);

         double ComputeTotalValue();

         void Clear();

         IList<CartLine> Lines { get; set; }
    }
}
