﻿using System.Linq;

namespace TechStoreGen2.Models
{
    public interface IOrderRepository
    {
        IQueryable<Order> Orders { get; }
        void SaveOrder(Order order);
        Order DeleteOrder(int orderID);
    }
}
