﻿namespace TechStoreGen2.Models
{
    public class OrderDetails
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int ProductID { get; set; }
        public int Count { get; set; }
        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; } 
    }
}
