﻿using TechStoreGen2.Models;

namespace TechStoreGen2.ViewModels
{
    public class CartIndexViewModel
    {
        public ICart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}
